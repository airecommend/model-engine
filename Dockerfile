FROM rstudio/plumber

RUN R -e ".libPaths(c( './Rlib', .libPaths()))"
RUN R -e "install.packages(c('jsonlite','abc','RSQLite','DBI','digest', 'keras'))"
RUN R -e "reticulate::install_miniconda()"
RUN R -e "library(keras); install_keras()"

ADD api.R /api.R
ADD funs.R /funs.R
ADD run.sh /run.sh

ENTRYPOINT ["/bin/bash","./run.sh"]
