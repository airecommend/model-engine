model-engine is the model host, it slurps a model if it can be found in the specified volume (air in this example), therefore, it needs to load after train-engine. model-engine just exposes one api - proposeInteraciton.

- build docker
`docker build . -t 'model-engine'`

- run docker
`docker run --rm -v air:/data -p 8000:8000 model-engine`

- call prediction service
`curl -H "Content-Type: application/json" -X POST -d '{"items":["11","22","33"],"k":3}' http://127.0.0.1:8000/proposeInteraction`

#full pipeline after 3 images are build and a volume to store data created
 - `docker run --rm -v air:/data -p 8001:8001 db-engine` launch db
 - `curl -H "Content-Type: application/json" -X POST -d @dat.json http:/127.0.0.1:8001/recordInteraction` feed in data
 - `run --rm -v air:/data train` train the model
 - `docker run --rm -v air:/data -p 8000:8000 air` launch model-engine
 - `curl -H "Content-Type: application/json" -X POST -d '{"items":["900","934"],"k":3}' http://127.0.0.1:8000/proposeInteraction` run prediction again

 OPTIONAL - this will be handled by the container-manager
 - `docker ps` find out id of the container with model-engine
 - `docker restart [ID of the container]` run new instance of the model-engine to slurp a model file
 - `curl -H "Content-Type: application/json" -X POST -d '{"items":["900","934"],"k":3}' http://127.0.0.1:8000/proposeInteraction` run prediction again
