#!/usr/bin/env bash

#run app
Rscript -e 'app <- plumber::plumb("./api.R"); app$run(host="0.0.0.0", port=8000, swagger=T)'
